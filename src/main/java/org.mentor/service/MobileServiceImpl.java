package org.mentor.service;


import org.joda.time.DateTime;
import org.mentor.dao.*;
import org.mentor.domain.Mobile;
import org.mentor.domain.MobileMaker;
import org.mentor.domain.MobileTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MobileServiceImpl implements MobileService {

    @Autowired
    @Qualifier(value="mobileJpaDao")
    private MobileJpaDao mobileDao;

    @Autowired
    @Qualifier(value="mobileMakerJpaDao")
    private MobileMakerJpaDao mobileMakerDao;

    @Autowired
    @Qualifier(value="mobileTransactionJpaDao")
    private MobileTransactionJpaDao mobileTransactionDao;


    public MobileServiceImpl() {

        /*mobileDao = new MobileJpaDao();
        mobileMakerDao = new MobileMakerJpaDao();
        mobileTransactionDao = new MobileTransactionJpaDao();*/

    }

    public void addMobile(Mobile mobile) {
        mobile.setPrice(mobile.getPrice().add(mobile.getExtra()));
        mobileDao.add(mobile);
    }

    public Mobile getMobile(int id) {
        return mobileDao.getMobile(id);

    }

    public Mobile getMobile(String name){
        return mobileDao.getMobile(name);
    }

    public List<Mobile> getAllMobiles() {
        return mobileDao.getAllMobiles();
    }


    public void addMobileMaker(MobileMaker mobileMaker) {

        mobileMakerDao.add(mobileMaker);
    }

    public MobileMaker getMobileMaker(int makerNumber) {
        return mobileMakerDao.getMobileMaker(makerNumber);

    }

    public void setQuantity(Mobile mobile) {
        mobile.setQuantity(0);

        mobileDao.add(mobile);
    }


    public void addTransaction(MobileTransaction mobileTransaction) {
        mobileTransactionDao.add(mobileTransaction);
    }


    public Mobile findMobileByName(String name) {
        return mobileDao.findMobileByName(name);
    }

    public Mobile saleMobile(int id) {
        return mobileDao.saleMobile(id);
    }


    public void setMobileDao(MobileJpaDao mobileDao) {
        this.mobileDao = mobileDao;
    }

    public MobileDao getMobileDao() {
        return mobileDao;
    }

    public void setMobileMakerDao(MobileMakerJpaDao mobileMakerDao) {
        this.mobileMakerDao = mobileMakerDao;
    }

    public MobileMakerDao getMobileMakerDao() {
        return mobileMakerDao;
    }

    public void setMobileTransactionDao(MobileTransactionJpaDao mobileTransactionDao) {
        this.mobileTransactionDao = mobileTransactionDao;
    }

    public MobileTransactionDao getMobileTransactionDao() {
        return mobileTransactionDao;
    }
}
