package org.mentor.service;


import org.joda.time.DateTime;
import org.mentor.domain.Mobile;
import org.mentor.domain.MobileMaker;
import org.mentor.domain.MobileTransaction;

import java.util.List;
import java.util.Set;

public interface MobileService {
    public void addMobile(Mobile mobile);

    public Mobile getMobile(int id);

    public List<Mobile> getAllMobiles();

    public void addMobileMaker(MobileMaker mobileMaker);

    public MobileMaker getMobileMaker(int makerNumber);

    public void addTransaction(MobileTransaction mobile);

    public Mobile findMobileByName(String name);

    public Mobile saleMobile(int id);

    public void setQuantity(Mobile mobile);
    public Mobile getMobile(String name);

}
