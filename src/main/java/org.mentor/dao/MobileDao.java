package org.mentor.dao;

import org.mentor.domain.Mobile;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface MobileDao {
    void add(Mobile mobile);

    public Mobile getMobile(int id);

    public List<Mobile> getAllMobiles();

    public Mobile findMobileByName(String name);

    public Mobile saleMobile(int id);

    public void setQuantity(Mobile mobile);
    public Mobile getMobile(String name);
}
