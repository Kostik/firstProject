package org.mentor.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.mentor.domain.Mobile;
import org.mentor.domain.MobileTransaction;
import org.mentor.util.HibernateUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Created by Skirdovs on 03.11.2015.
 */

@Repository
@Qualifier(value="mobileTransactionJpaDao")
public class MobileTransactionJpaDao implements MobileTransactionDao {
    public void add(MobileTransaction mobileTransaction) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.save(mobileTransaction);
            session.getTransaction().commit();
            session.close();
        } catch(HibernateException x) {
            session.close();
            throw new HibernateException("Could not add transaction " + mobileTransaction, x);
        }
    }

}
