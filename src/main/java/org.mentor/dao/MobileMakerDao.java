package org.mentor.dao;

import org.mentor.domain.MobileMaker;

/**
 * Created by Skirdovs on 28.10.2015.
 */
public interface MobileMakerDao {
    void add(MobileMaker mobileMaker);

    public MobileMaker getMobileMaker(int makerNumber);
}
