package org.mentor.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.mentor.domain.Mobile;
import org.mentor.util.HibernateUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.awt.print.Book;
import java.util.*;

@Repository
@Qualifier(value="mobileJpaDao")
public class MobileJpaDao implements MobileDao {
    public void add(Mobile mobile) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.save(mobile);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not add mobile " + mobile, x);
        }

    }

    public Mobile getMobile(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Mobile mobile = (Mobile) session.get(Mobile.class, new Integer(id));
            session.close();
            return mobile;
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not get mobile " + x);
        }
    }

    public List<Mobile> getAllMobiles() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            List mobiles = new ArrayList<Mobile>();
            mobiles = session.createCriteria(Mobile.class).list();
            session.close();
            return mobiles;
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not get all mobiles " + x);
        }
    }

    public Mobile findMobileByName(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.getNamedQuery("Mobile.findByName")
                    .setString("model", name);
            List<Mobile> result = query.list();
            session.close();
            return result.get(0);
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not find mobile " + name, x);
        }
    }

    public Mobile getMobile(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Mobile mobile = new Mobile();
        try {
            Query q = session.createQuery("from Mobile where model = '" + name + "' ");
            List<Mobile> mobiles = q.list();
            if (q != null) {
                mobile = mobiles.get(0);
            }
            session.close();
            return mobile;
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not get mobile " + name, x);
        }
    }

    public Mobile saleMobile(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Mobile mobile;
        try {
            session.beginTransaction();
            Query query = session.getNamedQuery("Mobile.findById")
                    .setInteger("id", id);
            List<Mobile> result = query.list();
            session.close();
            return result.get(0);
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not sale mobile " + x);
        }
    }

    public void setQuantity(Mobile mobile) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(mobile);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException x) {
            session.close();
            throw new HibernateException("Could not set quantity " + mobile, x);
        }
    }
}




