package org.mentor.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.mentor.domain.MobileMaker;
import org.mentor.util.HibernateUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Created by Skirdovs on 28.10.2015.
 */

@Repository
@Qualifier(value="mobileMakerJpaDao")
public class MobileMakerJpaDao implements MobileMakerDao {
    public void add(MobileMaker mobileMaker) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.save(mobileMaker);
            session.getTransaction().commit();
            session.close();
        } catch(HibernateException x) {
            session.close();
            throw new HibernateException("Could not add phone maker " + mobileMaker, x);
        }
    }

    public MobileMaker getMobileMaker(int makerNumber) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            MobileMaker mobileMaker = (MobileMaker) session.get(MobileMaker.class, new Integer(makerNumber));
            session.close();
            return mobileMaker;
            } catch(HibernateException x) {
            session.close();
            throw new HibernateException("Could not get mobile maker, id = " + makerNumber, x);
        }
    }


}
