package org.mentor.dao;

import org.joda.time.DateTime;
import org.mentor.domain.Mobile;
import org.mentor.domain.MobileTransaction;

/**
 * Created by Skirdovs on 03.11.2015.
 */
public interface MobileTransactionDao {
    public void add(MobileTransaction mobileTransaction);


}
