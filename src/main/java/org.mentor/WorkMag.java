package org.mentor;



import org.hibernate.Session;

import org.joda.time.DateTime;
import org.mentor.config.Config;
import org.mentor.domain.Mobile;
import org.mentor.domain.MobileMaker;
import org.mentor.domain.MobileTransaction;
import org.mentor.service.*;
import org.mentor.util.HibernateUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;

public class WorkMag {
    public static void main(String[] args) throws IOException, InterruptedException {

        //ApplicationContext context = new ClassPathXmlApplicationContext("app-context.xml");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        MobileService mobileService = (MobileService) context.getBean("mobileServiceImpl");

        //create service
        //MobileService mobileService = new MobileServiceImpl();


        //create first phone maker - LG
        MobileMaker mobileMakerLg = new MobileMaker();
        mobileMakerLg.setMaker("LG");

        //create first phone and set its parameters
        Mobile mobile1 = new Mobile();
        mobile1.setModel("Y-50");
        mobile1.setMobileMaker(mobileMakerLg);
        mobile1.setQuantity(1);
        mobile1.setExtra(new BigDecimal(500.00));
        mobile1.setPrice(new BigDecimal(12299.00d));

        //create second phone and set its parameters
        Mobile mobile2 = new Mobile();
        mobile2.setModel("Y-90");
        mobile2.setMobileMaker(mobileMakerLg);
        mobile2.setQuantity(1);
        mobile2.setExtra(new BigDecimal(100.00));
        mobile2.setPrice(new BigDecimal(4999.00d));

        //add this maker and phones to tables
        Set<Mobile> mobileSetLg = new HashSet<Mobile>();
        mobileSetLg.add(mobile1);
        mobileSetLg.add(mobile2);
        mobileMakerLg.setMobile(mobileSetLg);
        mobileService.addMobileMaker(mobileMakerLg);
        System.out.println("Add new phone maker " + mobileMakerLg.getMaker() + " to table PHONE_MAKER");
        mobileService.addMobile(mobile1);
        System.out.println("Add new phone " + mobile1.getModel() + " to table PRODUCT");
        mobileService.addMobile(mobile2);



        //create second phone maker - SAMSUNG
        MobileMaker mobileMakerSamsung = new MobileMaker();
        mobileMakerSamsung.setMaker("SAMSUNG");

        //create third phone and set its parameters
        Mobile mobile3 = new Mobile();
        mobile3.setModel("Galaxy A-5");
        mobile3.setMobileMaker(mobileMakerSamsung);
        mobile3.setQuantity(1);
        mobile3.setExtra(new BigDecimal(200.00));
        mobile3.setPrice(new BigDecimal(7499.00d));


        //create fourth phone and set its parameters
        Mobile mobile4 = new Mobile();
        mobile4.setModel("Galaxy A-3");
        mobile4.setMobileMaker(mobileMakerSamsung);
        mobile4.setQuantity(1);
        mobile4.setExtra(new BigDecimal(100.00));
        mobile4.setPrice(new BigDecimal(4999.00d));

        //add this maker and phones to tables
        Set<Mobile> mobileSetSamsung = new HashSet<Mobile>();
        mobileSetSamsung.add(mobile3);
        mobileSetSamsung.add(mobile4);
        mobileMakerSamsung.setMobile(mobileSetSamsung);
        mobileService.addMobileMaker(mobileMakerSamsung);
        System.out.println("Add new phone maker " + mobileMakerSamsung.getMaker() + " to table PHONE_MAKER");
        mobileService.addMobile(mobile3);
        System.out.println("Add new phone " + mobile3.getModel() + " to table PRODUCT");
        mobileService.addMobile(mobile4);
        System.out.println("Add new phone " + mobile4.getModel() + " to table PRODUCT");

        //create third phone maker - IPhone
        MobileMaker mobileMakerIPhone = new MobileMaker();
        mobileMakerIPhone.setMaker("IPhone");

        //create fifth phone and set its parameters
        Mobile mobile5 = new Mobile();
        mobile5.setModel("5s");
        mobile5.setMobileMaker(mobileMakerIPhone);
        mobile5.setQuantity(1);
        mobile5.setExtra(new BigDecimal(200.00));
        mobile5.setPrice(new BigDecimal(12299.00d));


        //create sixth phone and set its parameters
        Mobile mobile6 = new Mobile();
        mobile6.setModel("6");
        mobile6.setMobileMaker(mobileMakerIPhone);
        mobile6.setQuantity(1);
        mobile6.setExtra(new BigDecimal(100.00));
        mobile6.setPrice(new BigDecimal(17000.00d));

        //add this maker and phones to tables
        Set<Mobile> mobileSetIPhone = new HashSet<Mobile>();
        mobileSetIPhone.add(mobile5);
        mobileSetIPhone.add(mobile6);
        mobileMakerSamsung.setMobile(mobileSetIPhone);
        mobileService.addMobileMaker(mobileMakerIPhone);
        System.out.println("Add new phone maker " + mobileMakerIPhone.getMaker() + " to table PHONE_MAKER");
        mobileService.addMobile(mobile5);
        System.out.println("Add new phone " + mobile5.getModel() + " to table PRODUCT");
        mobileService.addMobile(mobile6);
        System.out.println("Add new phone " + mobile6.getModel() + " to table PRODUCT");

        Mobile mobileByName1 = mobileService.findMobileByName("6");
        System.out.println(mobileByName1);

        MobileTransaction transaction1 = new MobileTransaction();
        transaction1.setModel(mobileByName1.getModel());
        transaction1.setProducer(mobileByName1.getMobileMaker() + "");
        transaction1.setDate(new DateTime().plusHours(2));


        Mobile mobileByName2 = mobileService.findMobileByName("Y-50");
        System.out.println(mobileByName2);

        MobileTransaction transaction2 = new MobileTransaction();
        transaction2.setModel(mobileByName2.getModel());
        transaction2.setProducer(mobileByName2.getMobileMaker() + "");
        transaction2.setDate(new DateTime().plusHours(2));


        mobileService.addTransaction(transaction1);
        mobileService.addTransaction(transaction2);




        //close session factory
        HibernateUtil.shutdown();
    }


}