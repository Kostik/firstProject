package org.mentor.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@NamedQueries({
        @NamedQuery(name = "Mobile.findByPrice",
                query = "from Mobile m where m.price < :price"),
        @NamedQuery(name = "Mobile.findByName",
                query = "from Mobile m where m.model = :model"),
        @NamedQuery(name = "Mobile.findById",
                query = "from Mobile m where m.id = :id")
})

@Entity
@Table(name = "PRODUCT")
public class Mobile {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mobile_seq")
    @SequenceGenerator(
            name = "mobile_seq",
            sequenceName = "PRODUCT_SEQ",
            allocationSize = 50
    )
    @Column(name = "ID")
    private int id;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "EXTRA")
    private BigDecimal extra;

    @ManyToOne
    private MobileMaker mobileMaker;


    public void setMobileMaker(MobileMaker mobileMaker) {
        this.mobileMaker = mobileMaker;
    }

    public MobileMaker getMobileMaker() {
        return mobileMaker;
    }

    public int getId() {
        return this.id;
    }

    public String getModel() {
        return model;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public BigDecimal getExtra() {
        return extra;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setExtra(BigDecimal extra) {
        this.extra = extra;
    }

    @Override
    public String toString() {
        return "Mobile{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", extra=" + extra +
                ", mobileMaker=" + mobileMaker +
                '}';
    }
}
