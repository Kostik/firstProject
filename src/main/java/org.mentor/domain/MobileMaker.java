package org.mentor.domain;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Skirdovs on 28.10.2015.
 */

@Entity
@Table(name = "PHONE_MAKER")
public class MobileMaker {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "maker_seq")
    @SequenceGenerator(
            name = "maker_seq",
            sequenceName = "PHONE_MAKER_SEQ",
            allocationSize = 50
    )
    @Column(name = "ID")
    private int id;

    @Column(name = "MAKER")
    private String makerName;

    @OneToMany(mappedBy = "mobileMaker")
    Set<Mobile> mobile;

    public Set<Mobile> getMobile() {
        return mobile;
    }

    public void setMobile(Set<Mobile> mobile) {
        this.mobile = mobile;
    }


    public String getMaker() {
        return makerName;
    }

    public int getIdMaker() {
        return id;
    }

    public void setIdMaker(int id) {
        this.id = id;
    }

    public void setMaker(String makerName) {
        this.makerName = makerName;
    }

    @Override
    public String toString() {
        return "" + makerName;
    }
}

