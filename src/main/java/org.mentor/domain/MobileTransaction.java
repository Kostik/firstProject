package org.mentor.domain;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Skirdovs on 03.11.2015.
 */

@Entity
@Table(name = "PHONE_TRANSACTIONS")
public class MobileTransaction {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_seq")
    @SequenceGenerator(
            name = "transaction_seq",
            sequenceName = "PHONE_TRANSACTION_SEQ",
            allocationSize = 50
    )
    @Column(name = "ID_TRANSACTION")
    private int idTrans;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "PRODUCER")
    private String producer;

    /*@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_transaction")
    private Date date;
*/
    @Column(name = "DATE_TRANSACTION")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime date;

    public int getIdTrans() {
        return idTrans;
    }

    public void setIdTrans(int idTrans) {
        this.idTrans = idTrans;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public DateTime getDate() {

        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "idTrans=" + idTrans +
                ", phone = " + producer + " " + model;
    }
}
