package org.mentor.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Skirdovs on 24.11.2015.
 */
@Configuration
@Import(value = {ConfigDao.class, ConfigService.class})
public class Config {
}
