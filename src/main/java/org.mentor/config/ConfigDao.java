package org.mentor.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Skirdovs on 24.11.2015.
 */

@Configuration
@ComponentScan("org.mentor.dao")
public class ConfigDao {
}
